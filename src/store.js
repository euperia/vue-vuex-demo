import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
import * as types from './mutation-types'

const vuexPersist = new VuexPersist({
  key: 'my-app',
  storage: localStorage
})

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [vuexPersist.plugin],
  state: {
      name: null
  },
  mutations: {
    [types.CHANGE_NAME] (state, name) {
      state.name = name
    }
  },
  getters: {
    name: state => state.name
  },
  actions: {
    async [types.ACTION_CHANGE_NAME] ({commit}, name) {
      await setTimeout(() => {
         commit(types.CHANGE_NAME, name + ' (changed)')
      }, 1000)
    }
  }
})
