# Basic Vue & Vuex app with mutations

## How this works

This is a very basic application showing how to use Vuex mutations and actions with Vue.

The HelloWorld app contains a data property called `inputName`.  This holds the value of the name for the input field.

The store is set up with a `name` property. There is a `getter` method to get the value of the `name` from the store.
There is an action set up to update the `name` value with the value passed.  We could call the mutation directly but this isn't asyncronous, so we do it via a vuex  `action`.

Finally, there is a `watch` method on the HelloWorld component that watches the value of `name` and updates `inputName` whenever `name` changes.

So, the pattern is:

* Component values have local data parameters
* Component values are updated with `watch` methods.
* Watch methods track the vuex data and update the local data parameters.
* Updating values is done by calling store actions.
* Actions run and commit changes to the store via Mutations.
